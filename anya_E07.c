#define FUSE_USE_VERSION 28
#include <stdbool.h>
#include <stdlib.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

const int inf = 1000;
static const char *dirpath = "/home/tegar/Downloads";
static const int TIME_SIZE = 30;
static const int LOG_SIZE = 1000;

bool isAnimeku_(const char *path) 
{
    int len = strlen(path);
    for(int i = 0; i < len - 8 + 1; i++) {
        if (path[i] == 'A' 
        && path[i+1] == 'n' 
        && path[i+2] == 'i' 
        && path[i+3] == 'm' 
        && path[i+4] == 'e'
        && path[i+5] == 'k' 
        && path[i+6] == 'u' 
        && path[i+7] == '_') 
        return true;
    }
    return false;
}

bool isIAN_(const char *path) 
{
    int len = strlen(path);
    for (int i = 0; i < len - 5 + 1; i++) {
        if (path[i] == 'I' 
        && path[i+1] == 'A' 
        && path[i+2] == 'N' 
        && path[i+4] == '_') 
        return true;
    }
    return false;
}

bool isNamdosaq_(const char *path) 
{
    int len = strlen(path);
    for(int i = len - 1; i > = 11; i--) {
        if (path[i-11] == 'n' 
        && path[i-10] == 'a' 
        && path[i-9] == 'm' 
        && path[i-8] == '_' 
        && path[i-7] == 'd' 
        && path[i-6] == 'o' 
        && path[i-5] == '-' 
        && path[i-4] == 's' 
        && path[i-3] == 'a' 
        && path[i-2] == 'q' 
        && path[i-1] == '_') 
        return true;
    }
    return false;
}

// Encode + Decode kombinasi Atbash + ROT13
void encodeAtRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

void decodeAtRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}

// Encode + Decode Vignere Chiper
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}

void decodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
        else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
}

// Log info, rename, warning
void logInfo(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "INFO::%s:%s::%s", waktu, cmd, des);
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}

void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}

void detailFile(const char *namaFileLengkap, char *nama, char *ekstensi) 
{
    int id=0, i=0;
    while(namaFileLengkap[i]) 
    {
        if(namaFileLengkap[i] == '.') break;
        nama[id++] = namaFileLengkap[i++];
    }
    nama[id] = '\0';
    id = 0;
    while(namaFileLengkap[i]) ekstensi[id++] = namaFileLengkap[i++];
    ekstensi[id] = '\0';
}

// Encode + Decode folder Animeku
int encodeFolder(const char *basePath, const char* folderName) 
{
    char encryptedName[512];
    strcpy(encryptedName, folderName);
    encodeAtRot(encryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, encryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFolder(const char *basePath, const char* folderName) 
{
    char decryptedName[512];
    strcpy(decryptedName, folderName);
    decodeAtRot(decryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, decryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

// Encode + Decode folder IAN
int encodeFolderIAN(const char *basePath, const char* folderName) 
{
    char encryptedName[512];
    strcpy(encryptedName, folderName);
    encodeVig(encryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, encryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFolderIAN(const char *basePath, const char* folderName) 
{
    char decryptedName[512];
    strcpy(decryptedName, folderName);
    decodeVig(decryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, decryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

// Encode + Decode file Animeku
int encodeFile(char *basePath, char *name) 
{
    char fileName[512], ext[64];
    detailFile(name, fileName, ext);
    encodeAtRot(fileName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFile(char *basePath, char *name) 
{
    char fileName[1024], ext[64];
    detailFile(name, fileName, ext);
    decodeAtRot(fileName);
    char f_path[1024], t_path[1100];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

// Encode + Decode file IAN
int encodeFileIAN(char *basePath, char *name) 
{
    char fileName[512], ext[64];
    detailFile(name, fileName, ext);
    encodeVig(fileName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFileIAN(char *basePath, char *name) 
{
    char fileName[1024], ext[64];
    detailFile(name, fileName, ext);
    decodeVig(fileName);
    char f_path[1024], t_path[1100];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

// Rekursif Animeku
int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int count=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            count += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}

int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int count = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            count += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}

// Rekursif IAN
int encodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int count=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            count += encodeFolderRekursifIAN(path, depth - 1),
            encodeFolderIAN(basePath, dp->d_name);
        else if(encodeFileIAN(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}

int decodeFolderRekursifIAN(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int count = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            count += decodeFolderRekursifIAN(path, depth - 1),
            decodeFolderIAN(basePath, dp->d_name);
        else if(decodeFileIAN(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", direktori, path);
    res = lstat(fpath, stbuf);
    if(res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    dp = opendir(fpath);
    if (dp == NULL) return -errno;
    while((de = readdir(dp)) != NULL) 
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino; st.st_mode = de->d_type << 12;
        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path); 
    int fd = open(fpath, O_RDONLY), res;
    (void) fi;
    if (fd == -1) return -errno; 
    res = pread(fd, buf, size, offset); 
    if(res == -1)res = -errno; 
    close(fd);
    return res;
}

static int xmp_rename(const char *from, const char *to) 
{
    int res; 
    char fpath[1000], tpath[1000];
    if (strcmp(from, "/") == 0) from = direktori, sprintf(fpath, "%s", from);
    else sprintf(fpath, "%s%s", direktori, from);
    if (strcmp(to, "/") == 0) to = direktori, sprintf(tpath, "%s", to);
    else sprintf(tpath, "%s%s", direktori, to); 
    struct stat path_stat;
    stat(fpath, &path_stat);
    if (!S_ISREG(path_stat.st_mode)) 
    {
        // jika folder terencode dan mau didecode & rename Animeku ke IAN
        if (isAnimeku_(fpath) && isNamdosaq_(tpath)) 
        {
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }else if(isAnimeku_(fpath) && isIAN_(tpath)){
            decodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }
        // Rename IAN ke Animeku
        else if (isIAN_(fpath) && isAnimeku_(tpath)){
            encodeFolderRekursif(fpath,0);
            sistemLog(fpath,tpath,1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (isNamdosaq_(fpath) && isAnimeku_(tpath)) 
        {
            encodeFolderRekursif(fpath, 0);
            sistemLog(fpath, tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }
        else if (isAnimeku_(fpath) && !isAnimeku_(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int count = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", count);
        }
        // Jika folder terdecode dan mau diencode
        else if (!isAnimeku_(fpath) && isAnimeku_(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int count = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", count);
        //mulai no 2
        }else if(isAnimeku_(fpath) && isIAN_(tpath)){
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }else if(isNamdosaq_(fpath) && isIAN_(tpath)){
            encodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath,tpath, 1);
            printf("[Mengenkode %s dengan kedalaman = 0.]\n", tpath);
        }else if(isIAN_(fpath) && isAnimeku_(tpath)){
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }else if(isIAN_(fpath) && isNamdosaq_(tpath)){
            decodeFolderRekursifIAN(fpath, 0);
            sistemLog(fpath, tpath, 2);
            printf("[Mendekode %s dengan kedalaman = 0.]\n", fpath);
        }else if(isIAN_(fpath) && !isIAN_(tpath)){
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int count = decodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", count);
        }else if(!isIAN_(fpath) && isIAN_(tpath)){
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int count = encodeFolderRekursifIAN(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", count);
        }
    }
    res=rename(fpath, tpath);
    if(res == -1) return -errno;
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
	int res;  
	if (S_ISREG(mode)) 
    {
		res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0) res = close(res);
	} 
    else if (S_ISFIFO(mode)) res = mkfifo(fpath, mode);
	else res = mknod(fpath, mode, rdev);
	if (res == -1) return -errno; 
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode) 
{
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
    res = mkdir(fpath, mode);
    if (res == -1) return -errno;
    if (isAnimeku_(fpath)) sistemLog("", fpath, 3);
    else if(isIAN_(fpath)) sistemLog("", fpath, 3);
    return 0;
}

static int xmp_rmdir(const char *path){
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
    res = rmdir(fpath);
    if (res == -1) return -errno;
    sistemLog("",fpath, 4);
    return 0;
}

static int xmp_unlink(const char *path){
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
    res = unlink(fpath);
    if (res == -1) return -errno;
    sistemLog("",fpath, 5);
    return 0;
}

static const struct fuse_operations xmp_oper = 
{
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .mknod = xmp_mknod,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
