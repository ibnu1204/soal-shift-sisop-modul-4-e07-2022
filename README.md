# soal-shift-sisop-modul-4-E07-2022
### Anggota Kelompok E07
|Nama|NRP|
|--------------|--------------|
|Tegar Ganang Satrio Priambodo|5025201002|
|Rafael Asi Kristanto Tambunan|5025201168|
|Ahmad Ibnu Malik Rahman|5025201232|

## Soal 1
1a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

`isAnimeku_` digunakan untuk mengecek suatu directory apakah ada kata "Animeku_". Jika ada, maka akan di return `true` dan directory path yang diinputkan akan di encode. Begitu juga sebaliknya, jika tidak ada akan di return `false` dan akan di decode.
```
bool isAnimeku_(const char *path) 
{
    int len = strlen(path);
    for(int i = 0; i < len - 8 + 1; i++) {
        if (path[i] == 'A' 
        && path[i+1] == 'n' 
        && path[i+2] == 'i' 
        && path[i+3] == 'm' 
        && path[i+4] == 'e'
        && path[i+5] == 'k' 
        && path[i+6] == 'u' 
        && path[i+7] == '_') 
        return true;
    }
    return false;
}
```
Fungsi encode dan decode atbash chiper + rot13
```
void encodeAtRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

void decodeAtRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}
```

1b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

1c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

Jika terdapat directory atau file yang direname, maka akan dipanggil fungsi fuse `xmp_rename`. Disini akan dilakukan pengecekan apakah directory akan di encode atau di decode.
```
else if (isAnimeku_(fpath) && !isAnimeku_(tpath)) 
{
    printf("[Mendekode %s.]\n", fpath);
    sistemLog(fpath, tpath, 2);
    int count = decodeFolderRekursif(fpath, 1000);
    printf("[Total file yang terdekode: %d]\n", count);
}
else if (!isAnimeku_(fpath) && isAnimeku_(tpath)) 
{
    printf("[Mengenkode %s.]\n", fpath);
    sistemLog(fpath, tpath, 1);
    int count = encodeFolderRekursif(fpath, 1000);
    printf("[Total file yang terenkode: %d]\n", count);
}
```

1d. Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

Untuk mencatat aktivitas rename suatu directory, dibuat fungis `logRename` dan `sistemLog`.
```
void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    }    
}
```

1e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Membuat fungsi encode serta decode dari folder dan file “Animeku_”
```
int encodeFolder(const char *basePath, const char* folderName) 
{
    char encryptedName[512];
    strcpy(encryptedName, folderName);
    encodeAtRot(encryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, encryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFolder(const char *basePath, const char* folderName) 
{
    char decryptedName[512];
    strcpy(decryptedName, folderName);
    decodeAtRot(decryptedName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, folderName);
    sprintf(t_path, "%s/%s", basePath, decryptedName);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int encodeFile(char *basePath, char *name) 
{
    char fileName[512], ext[64];
    detailFile(name, fileName, ext);
    encodeAtRot(fileName);
    char f_path[1024], t_path[1024];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}

int decodeFile(char *basePath, char *name) 
{
    char fileName[1024], ext[64];
    detailFile(name, fileName, ext);
    decodeAtRot(fileName);
    char f_path[1024], t_path[1100];
    sprintf(f_path, "%s/%s", basePath, name);
    sprintf(t_path, "%s/%s%s", basePath, fileName, ext);
    int res = rename(f_path, t_path);
    if (res == -1) return -errno;
    return 0;
}
```
Setelah itu dibuat fungsi rekursif untuk membuat perulangan dari fungsi encode dan decode sebelumnya
```
int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int count=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            count += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}

int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int count = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            count += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) count++;
    }
    closedir(dir);
    return count;
}
```
Hasil pengerjaan

![messageImage_1652460170822](/uploads/60ce7daca028d2852ac9f33c61caddec/messageImage_1652460170822.jpg)


![image](/uploads/b667aedefaac6c2717324b65451ee332/image.png)

## Soal 2
2a. Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

`isIAN_` digunakan untuk mengecek suatu directory apakah ada kata "isIAN_". Jika ada, maka akan di return `true` dan directory path yang diinputkan akan di encode. Begitu juga sebaliknya, jika tidak ada akan di return `false` dan akan di decode.
```
bool isIAN_(const char *path) 
{
    int len = strlen(path);
    for (int i = 0; i < len - 5 + 1; i++) {
        if (path[i] == 'I' 
        && path[i+1] == 'A' 
        && path[i+2] == 'N' 
        && path[i+4] == '_') 
        return true;
    }
    return false;
}
```
Fungsi encode dan decode vignere chiper dengan key “INNUGANTENG” (case sensitive)
```
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}

void decodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
        else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
}
```

2b. Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

2c. Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

Jika terdapat directory atau file yang direname, maka akan dipanggil fungsi fuse `xmp_rename`. Disini akan dilakukan pengecekan apakah directory akan di encode atau di decode.
```
else if(isIAN_(fpath) && !isIAN_(tpath))
{
    printf("[Mendekode %s.]\n", fpath);
    sistemLog(fpath, tpath, 2);
    int count = decodeFolderRekursifIAN(fpath, 1000);
    printf("[Total file yang terdekode: %d]\n", count);
}
else if(!isIAN_(fpath) && isIAN_(tpath))
{
    printf("[Mengenkode %s.]\n", fpath);
    sistemLog(fpath, tpath, 1);
    int count = encodeFolderRekursifIAN(fpath, 1000);
    printf("[Total file yang terenkode: %d]\n", count);
}
```

2d. Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

2e. Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO.

Untuk mencatat aktivitas rename suatu directory, dibuat fungis `logInfo`, `logWarning`, dan `sistemLog`.
```void logInfo(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "INFO::%s:%s::%s", waktu, cmd, des);
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}

void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}

void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logInfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logInfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    }    
}
```
Hasil pengerjaan 

![messageImage_1652460046002](/uploads/aa62b66f04d27f6d19bfc92b57105c3d/messageImage_1652460046002.jpeg)

Isi dari hayolongapain_E07.log

![image](/uploads/011ba089a9ffceb42264a1b63d5d7e58/image.png)


![image](/uploads/4f3c4c82831878d31f0b3f06309f9694/image.png)


